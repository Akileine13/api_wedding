'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Weddings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      surname: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING
      },
      present: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      adult: {
        type: Sequelize.JSON(),
      },
      children: {
        type: Sequelize.JSON(),
      },
      town_hall: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      meal: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      brunch: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      activity: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      remark: {
        type: Sequelize.STRING(1000)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('Weddings');
  }
};
FROM node:fermium-stretch as builder
## Install build toolchain, install node deps and compile native add-ons
ADD package.json .
RUN apt-get update && apt-get install -y python3 make gcc g++
RUN npm install

FROM node:fermium-stretch-slim as app

## Copy built node modules and binaries without including the toolchain
WORKDIR /usr/api
ADD . .
RUN mkdir node_modules && apt-get update
COPY --from=builder node_modules ./node_modules
RUN ls
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Wedding extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Wedding.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      type: DataTypes.STRING
    },
    surname: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING
    },
    phone: {
      type: DataTypes.STRING
    },
    present: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    adult: {
      type: DataTypes.JSON(),
    },
    children: {
      type: DataTypes.JSON(),
    },
    town_hall: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    meal: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    brunch: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    activity: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    remark: {
      type: DataTypes.STRING,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'Wedding',
  });
  return Wedding;
};
const Wedding = require("../models").Wedding;
const jwt = require("jsonwebtoken");
const { ACCESS_TOKEN_SECRET, LOGIN_PAGE, SENDGRID_API_KEY } = process.env;
const sgMail = require('@sendgrid/mail');

exports.login = (req, res) => {
  if(req.body.password === LOGIN_PAGE) {
    const token = jwt.sign(
      { },
      ACCESS_TOKEN_SECRET,
      { expiresIn: "3600s" }
    );  
    res.header("access-token", token).send(token);
  } else {
    res.status(400).send("Mauvais mot de passe")
  }
}

exports.inscription = (req, res) => {
  Wedding.create({
    name: req.body.name,
    surname: req.body.surname,
    email: req.body.email,
    phone: req.body.phone,
    present: req.body.present,
    adult: req.body.adult,
    children: req.body.children,
    town_hall: req.body.town_hall,
    meal: req.body.meal,
    brunch: req.body.brunch,
    activity: req.body.activity,
    remark: req.body.remark,
  })
    .then((results) => {
      res.status(200).send(results);
      sgMail.setApiKey(SENDGRID_API_KEY);
      const msg = {
        to: 'laetitia.aronicacollaud@yahoo.fr',
        from: 'pierre@heraud.dev',
        subject: 'Inscription mariage',
        text: results.dataValues.present ? results.dataValues.name + ' ' + results.dataValues.surname + ' s\'est inscrit à notre mariage' : results.dataValues.name + ' ' + results.dataValues.surname + ' ne sera pas présent à notre mariage',
        html: results.dataValues.present ? results.dataValues.name + ' ' + results.dataValues.surname + ' s\'est inscrit à notre mariage' : results.dataValues.name + ' ' + results.dataValues.surname + ' ne sera pas présent à notre mariage',
      }
      sgMail
      .send(msg)
      .then((response) => {
        console.log(response[0].statusCode)
        console.log(response[0].headers)
      })
      .catch((error) => {
        console.error(error)
      })
    })
    .catch((err) => {
      res.status(500).send(err);
    });
}
const router = require("express").Router();
const ifToken = require("../config/ifToken");
const weddingController = require("../controllers/weddingController");

router.post('/login', weddingController.login);
router.post('/inscription', ifToken, weddingController.inscription);

module.exports = router;